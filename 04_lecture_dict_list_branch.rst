Why command line
----------------

- Powerful
- Easy to copy/paste, no screenshot
- Easy to guide other
- Understand command line will allow using GUI app
- basic CLI: http://www.familug.org/2012/03/ccgu-cat-cut-grep-uniq.html

Tuple
-----

- Immutable
- When list, when tuple

Comparision
-----------

- ``is`` True/False/None
- ``==`` for everything else
- ``x = range(4); y = x; x is y``
- ``id()``

Import
------

- import antigravity
- xkcd.com, news.ycombinator.com
